import React from 'react';
import { Provider } from 'react-redux';
import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import store from './src/redux/store';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/es/integration/react';

const persistedStore = persistStore(store);

const Prayer = () => (
  <Provider store={store}>
    <PersistGate persistor={persistedStore} loading={null}>
      <App />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent(appName, () => Prayer);
