import React from 'react';
import { View, Modal, Alert } from 'react-native';
import AppText from 'components/AppText';
import IconAdd from 'components/Icon/IconAdd';
import ButtonIcon from 'components/ButtonIcon';
import { FontWeight } from 'const';
import { styles } from './styles';

interface ModalProps {
  isModalVisible: boolean;
  title?: string;
  children: React.ReactElement;
  onClose: () => void;
}

const ModalApp: React.FC<ModalProps> = ({
  isModalVisible,
  title = '',
  children,
  onClose,
}) => {
  return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ButtonIcon styles={styles.buttonClose} onPress={onClose}>
              <IconAdd styles={styles.buttonIconClose} width={25} height={25} />
            </ButtonIcon>
            <View>
              <AppText
                style={styles.headerContainer}
                fontWeight={FontWeight.BOLD}>
                {title}
              </AppText>
            </View>

            {children}
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default ModalApp;
