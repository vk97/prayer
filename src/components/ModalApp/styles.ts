import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 30,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 5,
  },
  headerContainer: {
    marginBottom: 20,
  },
  buttonClose: {
    position: 'absolute',
    right: 15,
    top: 15,
  },
  buttonIconClose: {
    transform: [
      {
        rotate: '45deg',
      },
    ],
  },
});
