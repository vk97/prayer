import React, { ReactElement } from 'react';
import { TextInput, View } from 'react-native';
import baseStyles from 'styles/base';
import { styles } from './styles';
interface IconInputProps {
  value?: string;
  stylesProps?: {};
  placeholder: string;
  children: ReactElement;
  onChange: (event: any) => void;
  onSubmit: () => void;
  [x: string]: any;
}

const InputAdd: React.FC<IconInputProps> = ({
  value = '',
  placeholder,
  onChange,
  styleContainer = {},
  styleInput,
  onSubmit,
  children,
  ...inputProperty
}) => {
  return (
    <View style={[styles.inputContainer, styleContainer]}>
      {children}
      <TextInput
        value={value}
        placeholder={placeholder}
        style={{ ...baseStyles.input, ...styles.input, ...styleInput }}
        onChangeText={onChange}
        {...inputProperty}
        onSubmitEditing={onSubmit}
      />
    </View>
  );
};

export default InputAdd;
