import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  inputContainer: {
    display: 'flex',
    width: '100%',
  },
  input: {
    paddingLeft: 52,
  },
});
