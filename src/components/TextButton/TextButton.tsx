import React from 'react';
import { TouchableOpacity } from 'react-native';
import AppText from '../AppText';
import { styles } from './styles';

interface TextButtonProps {
  title: string;
  style?: {};
  onPress: () => void;
}

const TextButton: React.FC<TextButtonProps> = ({
  title,
  style = {},
  onPress,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.container, style]}
      onPress={onPress}>
      <AppText style={styles.text}>{title}</AppText>
    </TouchableOpacity>
  );
};

export default TextButton;
