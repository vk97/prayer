import React from 'react';
import { Text } from 'react-native';
import { FontWeight } from 'const';
import { styles } from './styles';

interface AppTextProps {
  style?: {};
  children: React.ReactNode;
  fontWeight?: FontWeight;
}

const AppText: React.FC<AppTextProps> = ({
  style = {},
  children,
  fontWeight = FontWeight.REGULAR,
}) => {
  let fontFamily = 'SFUIText-Regular';
  if (fontWeight === FontWeight.BOLD) {
    fontFamily = 'SFUIText-Bold';
  }
  return (
    <Text style={{ ...styles.default, ...style, fontFamily: fontFamily }}>
      {children}
    </Text>
  );
};

export default AppText;
