import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export const styles = StyleSheet.create({
  default: {
    fontSize: 17,
    lineHeight: 20,
    color: colors.silver400,
  },
});
