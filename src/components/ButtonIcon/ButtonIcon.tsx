import React from 'react';
import { TouchableOpacity } from 'react-native';

interface ButtonIconProps {
  onPress?: () => void;
  children: React.ReactNode;
  styles?: {};
}

const ButtonIcon: React.FC<ButtonIconProps> = ({
  onPress = () => {},
  children,
  styles = {},
}) => {
  return (
    <TouchableOpacity onPress={onPress} style={{ ...styles }}>
      {children}
    </TouchableOpacity>
  );
};

export default ButtonIcon;
