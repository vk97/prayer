import React from 'react';
import { View } from 'react-native';
import { Form, Field } from 'react-final-form';
import FieldButton from 'components/FieldButton';
import Input from 'components/Input';
import { NewColumn, RequestData } from 'types';
import { required } from 'utils/validation';

interface FormAddNewDataProps {
  onSubmitForm: (data: RequestData) => void;
  closeModal: () => void;
}

const FormAddNewData: React.FC<FormAddNewDataProps> = ({
  onSubmitForm,
  closeModal,
}) => {
  return (
    <View>
      <Form
        onSubmit={(data: NewColumn) => {
          onSubmitForm({ data });
        }}
        render={({ handleSubmit }) => (
          <View>
            <Field
              name="title"
              placeholder="Enter title desk..."
              validate={required}>
              {({ meta, placeholder, input }) => {
                return (
                  <Input
                    meta={meta}
                    placeholder={placeholder}
                    onChange={input.onChange}
                  />
                );
              }}
            </Field>
            <Field name="description" placeholder="Enter description desk...">
              {({ meta, placeholder, input }) => {
                return (
                  <Input
                    meta={meta}
                    placeholder={placeholder}
                    onChange={input.onChange}
                  />
                );
              }}
            </Field>
            <FieldButton
              title="Submit"
              onPress={() => {
                handleSubmit();
                closeModal();
              }}
            />
          </View>
        )}
      />
    </View>
  );
};

export default FormAddNewData;
