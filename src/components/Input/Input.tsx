import React from 'react';
import { TextInput, View } from 'react-native';
import AppText from '../AppText';
import baseStyles from 'styles/base';
import { FieldMetaState } from 'react-final-form';
import { styles } from './styles';

interface InputProps {
  meta: FieldMetaState<string>;
  placeholder: string;
  onChange: (event: any) => void;
  [x: string]: any;
}

const Input: React.FC<InputProps> = ({
  meta,
  placeholder,
  onChange,
  ...inputProperty
}) => {
  let styleInputError;
  let errorElement;

  if (meta.error && meta.touched) {
    styleInputError = styles.inputError;
    errorElement = <AppText style={styles.textError}>{meta.error}</AppText>;
  }

  return (
    <View style={styles.inputContainer}>
      <TextInput
        placeholder={placeholder}
        style={[baseStyles.input, styleInputError]}
        onChangeText={onChange}
        {...inputProperty}
      />
      {errorElement}
    </View>
  );
};

export default Input;
