import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  inputContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    marginBottom: 10,
  },
  inputError: {
    borderColor: colors.red,
  },
  textError: {
    paddingLeft: 5,
    fontSize: 13,
    lineHeight: 15,
    color: colors.red,
  },
});
