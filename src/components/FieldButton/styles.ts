import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export const styles = StyleSheet.create({
  buttonBeige: {
    width: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 15,
    backgroundColor: colors.beige,
    paddingTop: 8,
    paddingRight: 17,
    paddingBottom: 8,
    paddingLeft: 17,
  },
  buttonBeigeText: {
    fontFamily: 'SFUIText-Bold',
    fontSize: 12,
    lineHeight: 14,
    color: '#ffffff',
  },
});
