import React from 'react';
import { TouchableOpacity } from 'react-native';
import AppText from '../AppText';
import { styles } from './styles';

interface FieldButtonProps {
  title: string;
  style?: {};
  styleText?: {};
  onPress: () => void;
}

const FieldButton: React.FC<FieldButtonProps> = ({
  title,
  style = {},
  styleText = {},
  onPress,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.buttonBeige, style]}
      onPress={onPress}>
      <AppText style={{ ...styles.buttonBeigeText, ...styleText }}>
        {title.toUpperCase()}
      </AppText>
    </TouchableOpacity>
  );
};

export default FieldButton;
