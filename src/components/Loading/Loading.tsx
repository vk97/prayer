import React from 'react';
import { View } from 'react-native';
import AppText from 'components/AppText';

const Loading: React.FC = () => {
  return (
    <View>
      <AppText>Loading...</AppText>
    </View>
  );
};

export default Loading;
