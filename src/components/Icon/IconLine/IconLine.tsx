import React from 'react';
import { View } from 'react-native';
import Svg, { Rect } from 'react-native-svg';

interface IconLineProps {
  width?: number;
  height?: number;
  color?: string;
  styles?: {};
}

const IconLine: React.FC<IconLineProps> = ({
  styles = {},
  width = 3,
  height = 22,
  color = '#AC5253',
}) => {
  return (
    <View style={styles}>
      <Svg width={width} height={height} viewBox="0 0 3 23" fill="none">
        <Rect y="0.9375" width={width} height={height} rx="1.5" fill={color} />
      </Svg>
    </View>
  );
};

export default IconLine;
