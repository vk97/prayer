import React from 'react';
import { View } from 'react-native';
import Svg, { Rect } from 'react-native-svg';

interface IconCheckBoxOffProps {
  styles?: {};
  width?: number;
  height?: number;
  colorStroke?: string;
}

const IconCheckBoxOff: React.FC<IconCheckBoxOffProps> = ({
  styles = {},
  width = 22,
  height = 22,
  colorStroke = '#424E75',
}) => {
  return (
    <View style={styles}>
      <Svg width={width} height={height} viewBox="0 0 22 22" fill="none">
        <Rect
          x="0.5"
          y="0.5"
          width="21"
          height="21"
          rx="3.5"
          stroke={colorStroke}
        />
      </Svg>
    </View>
  );
};

export default IconCheckBoxOff;
