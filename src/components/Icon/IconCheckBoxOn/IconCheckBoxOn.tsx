import React from 'react';
import { View } from 'react-native';
import Svg, { Rect, Path } from 'react-native-svg';

interface IconCheckBoxOnProps {
  styles?: {};
  width?: number;
  height?: number;
  colorStroke?: string;
  colorCheck?: string;
}

const IconCheckBoxOn: React.FC<IconCheckBoxOnProps> = ({
  styles = {},
  width = 22,
  height = 22,
  colorStroke = '#424E75',
  colorCheck = '#514D47',
}) => {
  return (
    <View style={styles}>
      <Svg width={width} height={height} viewBox="0 0 22 22" fill="none">
        <Rect
          x="0.5"
          y="0.5"
          width="21"
          height="21"
          rx="3.5"
          stroke={colorStroke}
        />
        <Path
          d="M17 5L8.75 16L5 11"
          stroke={colorCheck}
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
      </Svg>
    </View>
  );
};

export default IconCheckBoxOn;
