import axios from 'axios';

const createAPI = () => {
  const api = axios.create({
    baseURL: 'http://trello-purrweb.herokuapp.com/',
    timeout: 1000 * 5,
    withCredentials: true,
  });

  return api;
};

export default createAPI;
