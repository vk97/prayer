import { emailPattern } from 'const';

export const required = (value?: string) => (value ? undefined : 'Required');
export const validEmail = (value?: string) => {
  return value && emailPattern.test(value) ? undefined : 'Email is Not Correct';
};

export const composeValidators = (...validators: any[]) => (value: string) =>
  validators.reduce((error, validator) => error || validator(value), undefined);
