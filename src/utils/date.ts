import { months } from 'const';

export const getDiffDate = (date: string) => {
  let second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;
  const dateProps: Date = new Date(date);
  const currentDate: Date = new Date();
  const timeDiff: number = currentDate.getTime() - dateProps.getTime();

  if (!isNaN(timeDiff)) {
    return Math.floor(timeDiff / day);
  }
};

export const getDate = (date: string) => {
  const dateProps: Date = new Date(date);
  const month = dateProps.getMonth();
  const day = dateProps.getUTCDate();
  const year = dateProps.getUTCFullYear();
  return months[month] + ' ' + day + ' ' + year;
};
