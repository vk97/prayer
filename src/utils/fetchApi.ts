import { Method } from 'axios';
import { Alert } from 'react-native';
import { UserAuth } from 'redux/user/types';
import { IUserData, NewCard, NewColumn, NewComment } from 'types';
import createAPI from './api';

type Data = NewColumn | NewCard | NewComment | UserAuth | IUserData;
type FetchApi = [Method, string, string, Data?];

const fetchApi = ([method, token, url, data]: FetchApi) => {
  let api = createAPI();

  api.interceptors.request.use((config) => {
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    config.method = method;
    config.data = data;
    return config;
  });

  api.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (!error.response) {
        Alert.alert('No internet connection. Try again');
        return error;
      }
    },
  );

  return api(url);
};

export default fetchApi;
