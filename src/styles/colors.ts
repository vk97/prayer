export default {
  beige: '#BFB393',
  blue: '#72A8BC',
  red: '#AC5253',
  silver100: '#E5E5E5',
  silver200: '#C8C8C8',
  silver300: '#9C9C9C',
  silver400: '#514D47',
};
