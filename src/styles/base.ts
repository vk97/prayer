import { StyleSheet } from 'react-native';
import colors from './colors';

export default StyleSheet.create({
  input: {
    width: '100%',
    paddingLeft: 20,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: colors.silver200,
    borderRadius: 10,
    color: colors.silver400,
    fontSize: 17,
    lineHeight: 20,
  },
  buttonRed: {
    color: '#ffffff',
    backgroundColor: colors.red,
  },
});
