import 'react-native-gesture-handler';
import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AuthStackNavigation from './stack/AuthStackNavigation';
import { lightTheme } from './themes/light';
import ColumnListsStackNavigation from './stack/ColumnsListStackNavigation';
import { selectUserData } from 'redux/user/selectors';

const RootStack = createStackNavigator();

const App: React.FC = () => {
  const userData = useSelector(selectUserData);
  const { token } = userData;
  const isUserAuth = !!token;

  return (
    <NavigationContainer theme={lightTheme}>
      <RootStack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        {isUserAuth ? (
          <RootStack.Screen
            name="ColumnListsStack"
            component={ColumnListsStackNavigation}
          />
        ) : (
          <RootStack.Screen name="AuthStack" component={AuthStackNavigation} />
        )}
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default App;
