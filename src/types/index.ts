export interface IUserData {
  id: number;
  email: string;
  name: string;
  token: null | string;
}

export enum AuthorizeStatus {
  USER_AUTH = 'USER_AUTH',
  USER_NO_AUTH = 'USER_NO_AUTH',
}

export interface IColumn {
  title: string;
  id: number;
  userId?: number;
  description?: string;
  user?: number;
}

export interface ICard {
  checked: boolean;
  columnId: number;
  commentsIds?: [];
  description: string;
  id: number;
  title: string;
}

export interface IComment {
  id: number;
  body: string;
  created: string;
  cardId: number;
  user?: IUserData;
  card?: ICard;
  userId?: number;
}

export interface NewColumn {
  title: string;
  description: string;
}

export interface NewCard {
  title: string;
  description: string;
  checked: boolean;
  column: IColumn;
}

export interface NewComment {
  body: string;
  created: string;
}

export interface RequestData {
  elementId?: number | string;
  data?: NewColumn | NewCard | NewComment;
}

export enum RequestType {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete',
}
