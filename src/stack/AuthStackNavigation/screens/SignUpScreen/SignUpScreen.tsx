import React from 'react';
import { View } from 'react-native';
import FormRegistration from '../../components/FormRegistration';
import { SignUpScreenNavigationProp } from './types';

interface SignUpScreenProps {
  navigation: SignUpScreenNavigationProp;
}

const SignUpScreen: React.FC<SignUpScreenProps> = ({ navigation }) => {
  return (
    <View>
      <FormRegistration navigation={navigation} />
    </View>
  );
};

export default SignUpScreen;
