import { StackNavigationProp } from '@react-navigation/stack';
import { AuthStackParamList } from 'stack/AuthStackNavigation/types';

export type SignUpScreenNavigationProp = StackNavigationProp<
  AuthStackParamList,
  'SignUp'
>;
