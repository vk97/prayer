import { StackNavigationProp } from '@react-navigation/stack';
import { AuthStackParamList } from 'stack/AuthStackNavigation/types';

export type SignInScreenNavigationProp = StackNavigationProp<
  AuthStackParamList,
  'SignIn'
>;
