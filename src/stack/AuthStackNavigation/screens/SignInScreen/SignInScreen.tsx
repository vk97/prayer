import React from 'react';
import { View } from 'react-native';
import FormAuth from '../../components/FormAuth';
import { SignInScreenNavigationProp } from './types';
interface SignInScreenProps {
  navigation: SignInScreenNavigationProp;
}

const SignInScreen: React.FC<SignInScreenProps> = ({ navigation }) => {
  return (
    <View>
      <FormAuth navigation={navigation} />
    </View>
  );
};

export default SignInScreen;
