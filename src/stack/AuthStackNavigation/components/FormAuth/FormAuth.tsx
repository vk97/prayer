import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Field } from 'react-final-form';
import { View } from 'react-native';
import TextButton from 'components/TextButton';
import FieldButton from 'components/FieldButton';
import Input from 'components/Input';
import AppText from 'components/AppText';
import { signInRequest } from 'redux/user/actions';
import { UserAuth } from 'redux/user/types';
import { styles } from './styles';
import { composeValidators, required, validEmail } from 'utils/validation';
import { changeStatusLoading } from 'redux/user/userSlice';
import Loading from 'components/Loading';
import { SignInScreenNavigationProp } from 'stack/AuthStackNavigation/screens/SignInScreen/types';
import { selectStatusLoadingUserAuth } from 'redux/user/selectors';

interface FormAuthProps {
  navigation: SignInScreenNavigationProp;
}

const FormAuth: React.FC<FormAuthProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const isLoadingUserData = useSelector(selectStatusLoadingUserAuth);

  if (isLoadingUserData) {
    return (
      <View style={styles.container}>
        <Loading />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <AppText style={styles.headerScreen}>Sign In</AppText>
      <Form
        onSubmit={(userData: UserAuth) => {
          dispatch(changeStatusLoading(true));
          dispatch(signInRequest(userData));
        }}
        render={({ handleSubmit }) => (
          <>
            <Field
              name="email"
              validate={composeValidators(required, validEmail)}
              placeholder="Enter email">
              {({ meta, placeholder, input }) => {
                return (
                  <Input
                    meta={meta}
                    placeholder={placeholder}
                    onChange={input.onChange}
                    autoCorrect={false}
                    autoCapitalize={'none'}
                    keyboardType={'email-address'}
                  />
                );
              }}
            </Field>
            <Field
              name="password"
              validate={required}
              placeholder="Enter password">
              {({ meta, placeholder, input }) => {
                return (
                  <Input
                    meta={meta}
                    placeholder={placeholder}
                    onChange={input.onChange}
                    secureTextEntry
                  />
                );
              }}
            </Field>
            <FieldButton
              title="Submit"
              onPress={handleSubmit}
              style={styles.buttonSignIn}
              styleText={styles.buttonTextSignIn}
            />
            <TextButton
              style={styles.buttonTextSignUp}
              title="Sign Up"
              onPress={() => navigation.navigate('SignUp')}
            />
          </>
        )}
      />
    </View>
  );
};

export default FormAuth;
