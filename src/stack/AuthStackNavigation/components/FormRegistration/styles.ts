import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'SFUIText-Bold',
  },
  headerScreen: {
    fontSize: 24,
    lineHeight: 26,
    fontWeight: '500',
    marginBottom: 25,
  },
  inputSignIn: {
    marginBottom: 15,
  },
  buttonSignIn: {
    marginRight: 0,
  },
  buttonTextSubmit: {
    fontSize: 18,
    lineHeight: 21,
    color: 'white',
  },
  buttonTextSignUp: {
    marginTop: 25,
    color: colors.blue,
  },
});
