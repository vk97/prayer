import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Field } from 'react-final-form';
import { View } from 'react-native';
import AppText from 'components/AppText';
import TextButton from 'components/TextButton';
import FieldButton from 'components/FieldButton';
import Input from 'components/Input';
import { signUpRequest } from 'redux/user/actions';
import { IUserData } from 'types';
import { styles } from './styles';
import { composeValidators, required, validEmail } from 'utils/validation';
import Loading from 'components/Loading';
import { changeStatusLoading } from 'redux/user/userSlice';
import { SignUpScreenNavigationProp } from 'stack/AuthStackNavigation/screens/SignUpScreen/types';
import { selectStatusLoadingUserAuth } from 'redux/user/selectors';

interface FormRegistrationProps {
  navigation: SignUpScreenNavigationProp;
}

const FormRegistration: React.FC<FormRegistrationProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const isLoadingUserData = useSelector(selectStatusLoadingUserAuth);

  if (isLoadingUserData) {
    return (
      <View style={styles.container}>
        <Loading />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <AppText style={styles.headerScreen}>Sign Up</AppText>
      <Form
        onSubmit={(userData: IUserData) => {
          dispatch(changeStatusLoading(true));
          dispatch(signUpRequest(userData));
        }}
        render={({ handleSubmit }) => (
          <>
            <Field name="name" validate={required} placeholder="Enter name">
              {({ meta, placeholder, input }) => {
                return (
                  <Input
                    meta={meta}
                    placeholder={placeholder}
                    onChange={input.onChange}
                  />
                );
              }}
            </Field>
            <Field
              name="email"
              validate={composeValidators(required, validEmail)}
              placeholder="Enter email">
              {({ meta, placeholder, input }) => {
                return (
                  <Input
                    meta={meta}
                    placeholder={placeholder}
                    onChange={input.onChange}
                    autoCorrect={false}
                    autoCapitalize={'none'}
                    keyboardType={'email-address'}
                  />
                );
              }}
            </Field>
            <Field
              name="password"
              validate={required}
              placeholder="Enter password">
              {({ meta, placeholder, input }) => {
                return (
                  <Input
                    meta={meta}
                    placeholder={placeholder}
                    onChange={input.onChange}
                    secureTextEntry
                  />
                );
              }}
            </Field>
            <FieldButton
              title="Submit"
              onPress={handleSubmit}
              style={styles.buttonSignIn}
              styleText={styles.buttonTextSubmit}
            />
            <TextButton
              style={styles.buttonTextSignUp}
              title={'Sign In'}
              onPress={() => navigation.navigate('SignIn')}
            />
          </>
        )}
      />
    </View>
  );
};

export default FormRegistration;
