import React from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import { selectColumnCards } from 'redux/todo/selectors';
import CardLists from 'stack/ColumnsListStackNavigation/components/CardLists';
import { ColumnScreenNavigationProp } from '../ColumnScreen/types';
import { styles } from './styles';

const mockColumn = { title: '123', id: 14 };
interface SubscribedScreenProps {
  navigation: ColumnScreenNavigationProp;
}

const SubscribedScreen: React.FC<SubscribedScreenProps> = ({ navigation }) => {
  const cardLists = useSelector(selectColumnCards(1300));

  return (
    <View style={styles.container}>
      <CardLists
        cards={cardLists}
        navigation={navigation}
        column={mockColumn}
      />
    </View>
  );
};

export default SubscribedScreen;
