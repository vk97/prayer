import AppText from 'components/AppText';
import ButtonIcon from 'components/ButtonIcon';
import IconAdd from 'components/Icon/IconAdd';
import IconBack from 'components/Icon/IconBack';
import IconLine from 'components/Icon/IconLine';
import IconPrayer from 'components/Icon/IconPrayer';
import { FontWeight } from 'const';
import React from 'react';
import { View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch } from 'react-redux';
import { updateCardRequest } from 'redux/todo/actions';
import CardColumnTable from 'stack/ColumnsListStackNavigation/components/CardColumnTable';
import CommentLists from 'stack/ColumnsListStackNavigation/components/CommentLists';
import FormChangeTitle from 'stack/ColumnsListStackNavigation/components/FormChangeTitle';
import ImageProfile from 'stack/ColumnsListStackNavigation/components/ImageProfile';
import colors from 'styles/colors';
import { styles } from './styles';
import { CardScreenNavigationProp, CardScreenRouteProp } from './types';

const mockCard = {
  added: '2020-12-27T10:05:21.000Z',
  prayedTotal: 123,
  mePrayed: 63,
  othersPrayed: 60,
  imageMembers: [
    'https://avatarko.ru/img/kartinka/31/zhivotnye_siluet_akula_30328.jpg',
    'https://avatarko.ru/img/avatar/15/zhivotnye_14774.jpg',
    'https://avatarko.ru/img/avatar/5/muzhchina_muzyka_4112.jpg',
  ],
};

interface CardScreenProps {
  route: CardScreenRouteProp;
  navigation: CardScreenNavigationProp;
}

const CardScreen: React.FC<CardScreenProps> = ({ route, navigation }) => {
  const dispatch = useDispatch();

  React.useLayoutEffect(() => {
    navigation.setOptions({
      header: () => (
        <View style={styles.header}>
          <View style={styles.headerTop}>
            <ButtonIcon onPress={() => navigation.goBack()}>
              <IconBack />
            </ButtonIcon>
            <ButtonIcon>
              <IconPrayer color="#fff" width={29} height={29} />
            </ButtonIcon>
          </View>
          <View style={[styles.headerTextContainer]}>
            <FormChangeTitle
              styleInput={
                route.params.checked
                  ? { ...styles.headerText, ...styles.headerTextChecked }
                  : styles.headerText
              }
              initialValue={route.params}
              onSubmit={(data) => {
                if (data) {
                  dispatch(
                    updateCardRequest({
                      elementId: route.params.id,
                      data,
                    }),
                  );
                }
              }}
            />
          </View>
        </View>
      ),
      headerStyle: styles.header,
    });
  });

  return (
    <ScrollView>
      <View style={styles.lastPrayerContainer}>
        <IconLine styles={styles.lineIcon} color={colors.red} />
        <AppText style={styles.lastPrayerText}>Last prayed 8 min ago</AppText>
      </View>

      <CardColumnTable card={mockCard} />

      <View style={[styles.container, styles.membersContainer]}>
        <AppText style={styles.headerBlock} fontWeight={FontWeight.BOLD}>
          Members
        </AppText>
        <View style={styles.imagesMembersContainer}>
          {mockCard.imageMembers.map((image, index) => (
            <ImageProfile
              url={image}
              width={33}
              height={33}
              stylesImageProps={styles.imageMember}
              key={image + `${index}`}
            />
          ))}
          <ButtonIcon styles={styles.buttonAddMember}>
            <IconAdd color="#fff" styles={styles.iconAddMembers} />
          </ButtonIcon>
        </View>
      </View>
      <View style={[styles.commentsContainer]}>
        <View style={[styles.container, styles.headerCommentsContainer]}>
          <AppText style={styles.headerBlock} fontWeight={FontWeight.BOLD}>
            Comments
          </AppText>
        </View>

        <CommentLists card={route.params} />
      </View>
    </ScrollView>
  );
};

export default CardScreen;
