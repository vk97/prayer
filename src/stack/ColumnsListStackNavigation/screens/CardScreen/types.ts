import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { ColumnsListsStackParamList } from 'stack/ColumnsListStackNavigation/types';

export type CardScreenRouteProp = RouteProp<ColumnsListsStackParamList, 'Card'>;

export type CardScreenNavigationProp = StackNavigationProp<
  ColumnsListsStackParamList,
  'Card'
>;
