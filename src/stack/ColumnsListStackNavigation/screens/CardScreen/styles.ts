import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.beige,
    paddingHorizontal: 15,
    paddingTop: 20,
    paddingBottom: 23,
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
  },
  headerTop: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerTextContainer: {
    marginTop: 15,
  },
  headerText: {
    color: '#fff',
    fontSize: 17,
    lineHeight: 27,
  },
  headerTextChecked: {
    textDecorationLine: 'line-through',
  },
  container: {
    paddingHorizontal: 15,
  },
  headerBlock: {
    marginBottom: 15,
    color: colors.blue,
    fontSize: 13,
    lineHeight: 15,
    textTransform: 'uppercase',
  },
  lastPrayerContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  lineIcon: {
    marginRight: 5,
  },
  lastPrayerText: {
    lineHeight: 20,
  },
  membersContainer: {
    paddingVertical: 20,
  },
  imagesMembersContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageMember: {
    borderRadius: 1000,
    overflow: 'hidden',
    marginRight: 7,
  },
  iconAddMembers: {
    left: 8,
    top: 8,
  },
  buttonAddMember: {
    width: 32,
    height: 32,
    backgroundColor: colors.beige,
    borderRadius: 1000,
  },
  headerCommentsContainer: {
    borderBottomWidth: 1,
    borderColor: colors.silver100,
  },
  commentsContainer: {
    marginTop: 8,
  },
});
