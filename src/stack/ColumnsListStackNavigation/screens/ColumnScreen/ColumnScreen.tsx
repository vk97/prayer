import React from 'react';
import { View } from 'react-native';
import ButtonIcon from 'components/ButtonIcon';
import IconSettings from 'components/Icon/IconSettings';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'redux/reducer';
import { updateColumnRequest } from 'redux/todo/actions';
import ColumnTopTabNavigation from 'stack/ColumnsListStackNavigation/components/ColumnTopTabNavigation';
import FormChangeTitle from 'stack/ColumnsListStackNavigation/components/FormChangeTitle';
import { ColumnScreenNavigationProp, ColumnScreenRouteProp } from './types';
import { styles } from './styles';
import { selectUserData } from 'redux/user/selectors';
interface ColumnScreenProps {
  route: ColumnScreenRouteProp;
  navigation: ColumnScreenNavigationProp;
}

const ColumnScreen: React.FC<ColumnScreenProps> = ({ route, navigation }) => {
  const dispatch = useDispatch();
  const userData = useSelector(selectUserData);
  const { token } = userData;

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerStyle: styles.headerColumnStyle,
      headerTitle: () => {
        return (
          <View style={styles.headerTitleContainer}>
            <FormChangeTitle
              initialValue={route.params}
              onSubmit={(data) => {
                if (token && data) {
                  dispatch(
                    updateColumnRequest({
                      token,
                      elementId: route.params.id,
                      data,
                    }),
                  );
                }
              }}
            />
          </View>
        );
      },
      headerRight: () => {
        return (
          <ButtonIcon onPress={() => {}} styles={styles.buttonHeaderRight}>
            <IconSettings />
          </ButtonIcon>
        );
      },
    });
  });

  return <ColumnTopTabNavigation route={route} navigation={navigation} />;
};

export default ColumnScreen;
