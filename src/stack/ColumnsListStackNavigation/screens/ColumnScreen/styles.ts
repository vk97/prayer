import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  headerColumnStyle: {
    elevation: 0,
    shadowOpacity: 0,
  },
  buttonHeaderRight: {
    right: 15,
  },
  headerTitleContainer: {
    width: '100%',
    maxWidth: 200,
  },
});
