import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { ColumnsListsStackParamList } from 'stack/ColumnsListStackNavigation/types';

export type ColumnScreenNavigationProp = StackNavigationProp<
  ColumnsListsStackParamList,
  'Column'
>;

export type ColumnScreenRouteProp = RouteProp<
  ColumnsListsStackParamList,
  'Column'
>;
