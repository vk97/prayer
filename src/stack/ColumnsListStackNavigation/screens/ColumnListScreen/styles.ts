import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    paddingHorizontal: 15,
  },
  buttonHeaderRight: {
    position: 'relative',
    top: 0,
    left: 0,
  },
});
