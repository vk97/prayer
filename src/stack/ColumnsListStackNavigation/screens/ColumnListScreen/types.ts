import { StackNavigationProp } from '@react-navigation/stack';
import { ColumnsListsStackParamList } from 'stack/ColumnsListStackNavigation/types';

export type ColumnListScreenNavigationProp = StackNavigationProp<
  ColumnsListsStackParamList,
  'ColumnLists'
>;
