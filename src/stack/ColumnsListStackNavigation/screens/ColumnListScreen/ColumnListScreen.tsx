import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Button, View } from 'react-native';
import ColumnLists from 'stack/ColumnsListStackNavigation/components/ColumnLists';
import { userLogout } from 'redux/user/actions';
import ButtonIcon from 'components/ButtonIcon';
import IconAdd from 'components/Icon/IconAdd';
import ModalApp from 'components/ModalApp';
import FormAddNewData from 'components/FormAddNewData';
import { createColumnRequest } from 'redux/todo/actions';
import { styles } from './styles';
import { ColumnListScreenNavigationProp } from './types';
import { RequestData } from 'types';

interface ColumnListScreenProps {
  navigation: ColumnListScreenNavigationProp;
}

const ColumnListScreen: React.FC<ColumnListScreenProps> = ({ navigation }) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const dispatch = useDispatch();
  const handleSubmitForm = (data: RequestData) => {
    dispatch(createColumnRequest(data));
  };

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={styles.headerContainer}>
          <ButtonIcon
            onPress={() => setModalVisible(true)}
            styles={styles.buttonHeaderRight}>
            <IconAdd />
          </ButtonIcon>
        </View>
      ),
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <ColumnLists navigation={navigation} />
      <Button title="Logout" onPress={() => dispatch(userLogout())} />
      <ModalApp
        title="Add new column"
        isModalVisible={isModalVisible}
        onClose={() => setModalVisible(false)}>
        <FormAddNewData
          onSubmitForm={handleSubmitForm}
          closeModal={() => setModalVisible(false)}
        />
      </ModalApp>
    </View>
  );
};

export default ColumnListScreen;
