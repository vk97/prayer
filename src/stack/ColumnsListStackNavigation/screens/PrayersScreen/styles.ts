import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 15,
  },
  containerPaddingHorizontal: {
    paddingHorizontal: 15,
  },
  formAddNewCard: {
    marginBottom: 38,
  },
  buttonChangeVisible: {
    paddingVertical: 20,
  },
  cardListsCompleted: {
    borderTopColor: colors.silver100,
    borderTopWidth: 1,
  },
});
