import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { View } from 'react-native';
import CardLists from 'stack/ColumnsListStackNavigation/components/CardLists';
import FormAdd from 'stack/ColumnsListStackNavigation/components/FormAdd';
import FieldButton from 'components/FieldButton';
import { selectColumnCards } from 'redux/todo/selectors';
import { IColumn } from 'types';
import { styles } from './styles';
import { ColumnScreenNavigationProp } from '../ColumnScreen/types';
interface PrayersScreenProps {
  currentColumn: IColumn;
  navigation: ColumnScreenNavigationProp;
}

const PrayersScreen: React.FC<PrayersScreenProps> = ({
  currentColumn,
  navigation,
}) => {
  const [isVisibleCompleted, setVisibleCompleted] = useState(false);
  const cardLists = useSelector(selectColumnCards(currentColumn.id));
  const cardNoChecked = cardLists?.filter((card) => card.checked === false);
  const cardChecked = cardLists?.filter((card) => card.checked === true);
  let cardCompletedElements;
  let buttonShowElement;

  cardCompletedElements = (
    <View style={styles.cardListsCompleted}>
      <CardLists
        column={currentColumn}
        cards={cardChecked}
        navigation={navigation}
      />
    </View>
  );

  buttonShowElement = (
    <>
      <View style={{ ...styles.buttonChangeVisible }}>
        <FieldButton
          title={
            isVisibleCompleted
              ? 'hide Answered Prayers'
              : 'Show Answered Prayers'
          }
          onPress={() => setVisibleCompleted(!isVisibleCompleted)}
        />
      </View>
      {isVisibleCompleted && cardCompletedElements}
    </>
  );

  return (
    <View style={styles.container}>
      <View style={styles.containerPaddingHorizontal}>
        <FormAdd column={currentColumn} />
      </View>
      {cardChecked.length > 0 ? (
        <CardLists
          column={currentColumn}
          cards={cardNoChecked}
          ListFooterComponent={buttonShowElement}
          navigation={navigation}
        />
      ) : (
        <CardLists
          column={currentColumn}
          cards={cardNoChecked}
          navigation={navigation}
        />
      )}
    </View>
  );
};

export default PrayersScreen;
