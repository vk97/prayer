import { ICard, IColumn } from 'types';

export type ColumnsListsStackParamList = {
  ColumnLists: undefined;
  Column: IColumn;
  Card: ICard;
};
