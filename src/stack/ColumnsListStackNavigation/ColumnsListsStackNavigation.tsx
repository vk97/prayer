import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ColumnListsScreen from './screens/ColumnListScreen';
import CardScreen from './screens/CardScreen';
import ColumnScreen from './screens/ColumnScreen';
import { styles } from './styles';
import { ColumnsListsStackParamList } from './types';
import { View } from 'react-native';

const ColumnListsStack = createStackNavigator<ColumnsListsStackParamList>();

const ColumnListsStackNavigation = () => {
  return (
    <ColumnListsStack.Navigator
      initialRouteName="ColumnLists"
      screenOptions={{
        headerStyle: styles.headerStyle,
        headerTitleStyle: styles.headerTitleStyle,
        headerTitleAlign: 'center',
        headerLeft: () => React.createElement(View),
      }}>
      <ColumnListsStack.Screen
        name="ColumnLists"
        component={ColumnListsScreen}
        options={{
          title: 'My Desk',
        }}
      />
      <ColumnListsStack.Screen name="Column" component={ColumnScreen} />
      <ColumnListsStack.Screen name="Card" component={CardScreen} />
    </ColumnListsStack.Navigator>
  );
};

export default ColumnListsStackNavigation;
