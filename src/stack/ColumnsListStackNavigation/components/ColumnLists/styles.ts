import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  container: {
    padding: 15,
    flex: 1,
  },
  rowBack: {
    alignItems: 'center',
    flex: 1,
    height: '100%',
    flexDirection: 'row',
    borderRadius: 4,
    overflow: 'hidden',
    marginBottom: 10,
    paddingLeft: 15,
    backgroundColor: colors.red,
  },
  backRightBtn: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    right: 0,
  },
  backRightBtnRight: {
    backgroundColor: colors.red,
    right: 0,
  },
});
