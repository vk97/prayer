import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, Animated, Dimensions } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import Column from '../Column';
import Loading from 'components/Loading';
import {
  getColumnListsRequest,
  getCardListsRequest,
  deleteColumnRequest,
  getCommentListsRequest,
} from 'redux/todo/actions';
import { styles } from './styles';
import ButtonSwipe from '../ButtonSwipe';
import { selectColumns } from 'redux/todo/selectors';
import { IColumn } from 'types';
import { ColumnListScreenNavigationProp } from 'stack/ColumnsListStackNavigation/screens/ColumnListScreen/types';
import {
  selectStatusLoadedCards,
  selectStatusLoadedColumns,
  selectStatusLoadedComments,
} from 'redux/application/selectors';
import { selectUserData } from 'redux/user/selectors';

interface RowTranslateValues {
  [key: string]: any;
}

interface ColumnListsProps {
  navigation: ColumnListScreenNavigationProp;
}

const ColumnLists: React.FC<ColumnListsProps> = ({ navigation }) => {
  const [isLoading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const columnLists = useSelector(selectColumns);
  const isLoadedColumns = useSelector(selectStatusLoadedColumns);
  const isLoadedCards = useSelector(selectStatusLoadedCards);
  const isLoadedComments = useSelector(selectStatusLoadedComments);
  const userData = useSelector(selectUserData);
  const isEmptyData = !isLoadedColumns && !isLoadedCards && !isLoadedComments;
  const { token } = userData;

  if (!isEmptyData && token && isLoading) {
    setLoading(false);
  }

  if (isEmptyData && token && !isLoading) {
    setLoading(true);
    dispatch(getColumnListsRequest());
    dispatch(getCardListsRequest());
    dispatch(getCommentListsRequest());
  }

  const rowTranslateAnimatedValues: RowTranslateValues = {};

  const onSwipeValueChange = ({
    key,
    value,
  }: {
    key: string;
    value: number;
  }) => {
    if (value < -Dimensions.get('window').width) {
      Animated.timing(rowTranslateAnimatedValues[key], {
        toValue: 0,
        duration: 200,
        useNativeDriver: false,
      }).start(() => {
        dispatch(deleteColumnRequest(key));
      });
    }
  };

  const renderItem = (data: { item: IColumn }) => {
    rowTranslateAnimatedValues[`${data.item.id}`] = new Animated.Value(60);

    return (
      <Column
        column={data.item}
        navigation={navigation}
        rowHeightAnimatedValue={rowTranslateAnimatedValues[data.item.id]}
      />
    );
  };

  const renderHiddenItem = (data: { item: IColumn }) => {
    return (
      <View style={[styles.rowBack]}>
        <View style={[styles.backRightBtn, styles.backRightBtnRight]}>
          <ButtonSwipe
            styleButton={{
              ...styles.backRightBtnRight,
            }}
            title="Delete"
            onPress={() => {
              dispatch(deleteColumnRequest(data.item.id));
            }}
          />
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <Loading />
      ) : (
        <SwipeListView
          data={columnLists}
          keyExtractor={(column) => column.id.toString()}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          leftOpenValue={75}
          rightOpenValue={-90}
          disableRightSwipe
          rightActivationValue={-200}
          rightActionValue={-500}
          onSwipeValueChange={onSwipeValueChange}
        />
      )}
    </View>
  );
};

export default ColumnLists;
