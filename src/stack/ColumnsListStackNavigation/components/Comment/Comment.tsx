import AppText from 'components/AppText';
import { FontWeight } from 'const';
import React from 'react';
import { View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { selectUserData } from 'redux/user/selectors';
import { IComment, IUserData } from 'types';
import { getDiffDate } from 'utils/date';
import ImageProfile from '../ImageProfile';
import { styles } from './styles';

const imageMembers = [
  'https://avatarko.ru/img/kartinka/31/zhivotnye_siluet_akula_30328.jpg',
  'https://avatarko.ru/img/avatar/15/zhivotnye_14774.jpg',
  'https://avatarko.ru/img/avatar/5/muzhchina_muzyka_4112.jpg',
];

interface CommentProps {
  comment: IComment;
  onLongPress: () => void;
}

const Comment: React.FC<CommentProps> = ({ comment, onLongPress }) => {
  const userData: IUserData = useSelector(selectUserData);
  const diffDateCreated = getDiffDate(comment.created);
  const daysCreated = diffDateCreated ? `${diffDateCreated} days ago` : 'today';

  return (
    <TouchableOpacity onLongPress={onLongPress} delayLongPress={3000}>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <ImageProfile url={imageMembers[1]} width={40} height={40} />
        </View>
        <View style={styles.containerCommentInfo}>
          <View style={styles.containerInfo}>
            <AppText style={styles.nameUserText} fontWeight={FontWeight.BOLD}>
              {userData.name}
            </AppText>
            <AppText style={styles.dataCreatedText}>{daysCreated}</AppText>
          </View>
          <View>
            <AppText>{comment.body}</AppText>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Comment;
