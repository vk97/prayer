import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 74,
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderColor: colors.silver100,
  },
  imageContainer: {
    marginRight: 10,
  },
  containerCommentInfo: {
    flex: 1,
  },
  containerInfo: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  nameUserText: {
    marginRight: 5,
  },
  dataCreatedText: {
    fontSize: 13,
    lineHeight: 16,
    color: colors.silver300,
  },
});
