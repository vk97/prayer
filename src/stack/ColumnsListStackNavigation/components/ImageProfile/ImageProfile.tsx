import React from 'react';
import { Image } from 'react-native-elements';
import { styles } from './styles';

interface ImageProfile {
  url: string;
  width: number;
  height: number;
  stylesImageProps?: {};
}

const ImageProfile: React.FC<ImageProfile> = ({
  url,
  width,
  height,
  stylesImageProps = {},
}) => {
  return (
    <Image
      source={{ uri: url }}
      style={{ width, height, ...styles.container, ...stylesImageProps }}
    />
  );
};

export default ImageProfile;
