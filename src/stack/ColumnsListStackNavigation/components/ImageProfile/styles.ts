import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    borderRadius: 1000,
    overflow: 'hidden',
  },
});
