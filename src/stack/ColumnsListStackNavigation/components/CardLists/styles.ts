import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  rowBack: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    overflow: 'hidden',
    paddingLeft: 15,
    backgroundColor: colors.red,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    right: 0,
  },
  backRightBtnRight: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.red,
    right: 0,
  },
});
