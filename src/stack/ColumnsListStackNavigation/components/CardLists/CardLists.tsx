import React from 'react';
import { Animated, Dimensions, View } from 'react-native';
import Card from '../Card';
import { ICard, IColumn } from 'types';
import { styles } from './styles';
import { SwipeListView } from 'react-native-swipe-list-view';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCardRequest } from 'redux/todo/actions';
import ButtonSwipe from '../ButtonSwipe';
import { ColumnScreenNavigationProp } from 'stack/ColumnsListStackNavigation/screens/ColumnScreen/types';
import { selectUserData } from 'redux/user/selectors';

interface CardListsProps {
  column: IColumn;
  cards: Array<ICard>;
  ListFooterComponent?: React.ReactElement;
  navigation: ColumnScreenNavigationProp;
}

interface RowTranslateValues {
  [key: string]: any;
}

const CardLists: React.FC<CardListsProps> = ({
  column,
  cards,
  ListFooterComponent = React.createElement(View),
  navigation,
}) => {
  const dispatch = useDispatch();
  const userData = useSelector(selectUserData);
  const { token } = userData;
  const rowTranslateAnimatedValues: RowTranslateValues = {};

  const onSwipeValueChange = ({
    key,
    value,
  }: {
    key: string;
    value: number;
  }) => {
    if (value < -Dimensions.get('window').width) {
      Animated.timing(rowTranslateAnimatedValues[key], {
        toValue: 0,
        duration: 200,
        useNativeDriver: false,
      }).start(() => {
        if (token) {
          dispatch(deleteCardRequest(key));
        }
      });
    }
  };

  const renderItem = (data: { item: ICard }) => {
    rowTranslateAnimatedValues[`${data.item.id}`] = new Animated.Value(68);

    return (
      <Card
        column={column}
        card={data.item}
        onPress={() => navigation.navigate('Card', data.item)}
      />
    );
  };

  const renderHiddenItem = (data: { item: ICard }) => {
    return (
      <View style={[styles.rowBack]}>
        <View style={[styles.backRightBtn, styles.backRightBtnRight]}>
          <ButtonSwipe
            styleButton={{
              ...styles.backRightBtnRight,
            }}
            title="Delete"
            onPress={() => {
              if (token) {
                dispatch(deleteCardRequest(data.item.id));
              }
            }}
          />
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <SwipeListView
        useFlatList
        data={cards}
        keyExtractor={(card) => card.id.toString()}
        renderItem={renderItem}
        renderHiddenItem={renderHiddenItem}
        rightOpenValue={-90}
        disableRightSwipe
        rightActivationValue={-200}
        rightActionValue={-500}
        onSwipeValueChange={onSwipeValueChange}
        ListFooterComponent={ListFooterComponent}
      />
    </View>
  );
};

export default CardLists;
