import React from 'react';
import { Animated, StyleProp, TouchableOpacity, ViewStyle } from 'react-native';
import AppText from 'components/AppText';
import { FontWeight } from 'const';
import { IColumn } from 'types';
import { styles } from './styles';
import { ColumnListScreenNavigationProp } from 'stack/ColumnsListStackNavigation/screens/ColumnListScreen/types';

interface ColumnProps {
  column: IColumn;
  navigation: ColumnListScreenNavigationProp;
  rowHeightAnimatedValue: Animated.AnimatedProps<StyleProp<ViewStyle>>;
}

const Column: React.FC<ColumnProps> = ({
  column,
  navigation,
  rowHeightAnimatedValue,
}) => {
  return (
    <Animated.View
      style={[styles.columnContainer, { minHeight: rowHeightAnimatedValue }]}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.column}
        onPress={() => navigation.navigate('Column', column)}>
        <AppText fontWeight={FontWeight.BOLD}>{column.title}</AppText>
      </TouchableOpacity>
    </Animated.View>
  );
};

export default Column;
