import React from 'react';
import { useDispatch } from 'react-redux';
import { View } from 'react-native';
import { Form, Field } from 'react-final-form';
import InputAdd from 'components/InputAdd';
import { createCardRequest } from 'redux/todo/actions';
import { styles } from './styles';
import IconAdd from 'components/Icon/IconAdd';
import { IColumn } from 'types';
import { required } from 'utils/validation';

interface FormAddProps {
  column: IColumn;
}

const FormAdd: React.FC<FormAddProps> = ({ column }) => {
  const dispatch = useDispatch();

  return (
    <View style={styles.inputAddContainer}>
      <Form
        onSubmit={({ title }) => {
          dispatch(
            createCardRequest({
              data: {
                title: title,
                description: '',
                checked: false,
                column,
              },
            }),
          );
        }}
        render={({ handleSubmit, form, values }) => (
          <View>
            <Field
              name="title"
              placeholder="Add a prayer..."
              validate={required}>
              {({ placeholder, input }) => {
                return (
                  <InputAdd
                    value={values.title}
                    placeholder={placeholder}
                    onChange={input.onChange}
                    onSubmit={() => {
                      handleSubmit();
                      form.reset();
                    }}>
                    <IconAdd width={24} height={24} styles={styles.iconAdd} />
                  </InputAdd>
                );
              }}
            </Field>
          </View>
        )}
      />
    </View>
  );
};

export default FormAdd;
