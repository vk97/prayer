import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  inputAddContainer: {
    marginBottom: 13,
  },
  iconAdd: {
    position: 'absolute',
    top: 13,
    left: 14,
    width: 24,
    height: 24,
    zIndex: 2,
  },
});
