import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  container: {
    width: '50%',
    minHeight: 108,
    paddingVertical: 26,
    paddingHorizontal: 15,
    justifyContent: 'center',
    borderColor: colors.silver100,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
  },
  totalText: {
    fontSize: 32,
    lineHeight: 38,
    color: colors.beige,
  },
  totalDateText: {
    fontSize: 22,
    lineHeight: 26,
    color: colors.beige,
  },
  totalTextTitle: {
    fontSize: 13,
    lineHeight: 15,
  },
  totalTextSubtitle: {
    fontSize: 13,
    lineHeight: 15,
    color: colors.blue,
  },
  containerTableData: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  noBorderBottom: {
    borderBottomWidth: 0,
  },
});
