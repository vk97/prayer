import AppText from 'components/AppText';
import React from 'react';
import { View } from 'react-native';
import { getDate, getDiffDate } from 'utils/date';
import { styles } from './styles';

interface CardColumnTableProps {
  card: {
    added: string;
    prayedTotal: number;
    mePrayed: number;
    othersPrayed: number;
  };
}

const CardColumnTable: React.FC<CardColumnTableProps> = ({ card }) => {
  const openedDays = getDiffDate(card.added);
  const date = getDate(card.added);
  let openDaysElement;

  if (openedDays) {
    openDaysElement = (
      <AppText
        style={
          styles.totalTextSubtitle
        }>{`Opened for ${openedDays} days`}</AppText>
    );
  }

  return (
    <View style={styles.containerTableData}>
      <View style={{ ...styles.container, ...styles.noBorderBottom }}>
        <AppText style={styles.totalDateText}>{date}</AppText>
        <AppText style={styles.totalTextTitle}>Date Added</AppText>
        {openDaysElement}
      </View>
      <View style={{ ...styles.container, ...styles.noBorderBottom }}>
        <AppText style={styles.totalText}>{card.prayedTotal}</AppText>
        <AppText style={styles.totalTextTitle}>Times Prayed Total</AppText>
      </View>
      <View style={styles.container}>
        <AppText style={styles.totalText}>{card.mePrayed}</AppText>
        <AppText style={styles.totalTextTitle}>Times Prayed by Me</AppText>
      </View>
      <View style={styles.container}>
        <AppText style={styles.totalText}>{card.othersPrayed}</AppText>
        <AppText style={styles.totalTextTitle}>Times Prayed by Others</AppText>
      </View>
    </View>
  );
};

export default CardColumnTable;
