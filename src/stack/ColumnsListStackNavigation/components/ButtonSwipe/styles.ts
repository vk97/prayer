import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.blue,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  buttonText: {
    color: 'white',
  },
});
