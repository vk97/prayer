import React from 'react';
import { TouchableOpacity } from 'react-native';
import AppText from 'components/AppText';
import { styles } from './styles';

interface ButtonSwipeProps {
  title?: string;
  styleButton?: {};
  styleButtonText?: {};
  onPress: () => void;
}

const ButtonSwipe: React.FC<ButtonSwipeProps> = ({
  title,
  styleButton,
  styleButtonText,
  onPress,
}) => {
  return (
    <TouchableOpacity
      style={{ ...styles.button, ...styleButton }}
      onPress={onPress}>
      <AppText style={{ ...styles.buttonText, ...styleButtonText }}>
        {title}
      </AppText>
    </TouchableOpacity>
  );
};

export default ButtonSwipe;
