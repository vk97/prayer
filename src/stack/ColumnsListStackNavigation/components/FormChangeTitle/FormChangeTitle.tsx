import React from 'react';
import { Form, Field } from 'react-final-form';
import { TextInput } from 'react-native-gesture-handler';
import { NewCard, NewColumn } from 'types';

interface FormChangeTitleProps {
  initialValue: {};
  onSubmit: (data: NewColumn | NewCard) => void;
  styleInput?: {};
}

const FormChangeTitle: React.FC<FormChangeTitleProps> = ({
  initialValue,
  onSubmit,
  styleInput = {},
}) => {
  return (
    <Form
      initialValues={initialValue}
      onSubmit={onSubmit}
      render={({ handleSubmit, values }) => (
        <Field name="title">
          {({ input }) => {
            return (
              <TextInput
                multiline={true}
                style={styleInput}
                value={values.title}
                onChange={input.onChange}
                onBlur={handleSubmit}
              />
            );
          }}
        </Field>
      )}
    />
  );
};

export default FormChangeTitle;
