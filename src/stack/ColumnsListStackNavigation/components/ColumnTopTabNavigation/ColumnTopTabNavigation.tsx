import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import SubscribedScreen from '../../screens/SubscribedScreen';
import PrayersScreen from '../../screens/PrayersScreen';
import colors from 'styles/colors';
import {
  ColumnScreenNavigationProp,
  ColumnScreenRouteProp,
} from 'stack/ColumnsListStackNavigation/screens/ColumnScreen/types';

const ColumnTopTab = createMaterialTopTabNavigator();

interface ColumnTopTabNavigationProps {
  route: ColumnScreenRouteProp;
  navigation: ColumnScreenNavigationProp;
}

const ColumnTopTabNavigation: React.FC<ColumnTopTabNavigationProps> = ({
  route,
  navigation,
}) => {
  return (
    <ColumnTopTab.Navigator
      swipeEnabled={false}
      initialRouteName="My prayers"
      tabBarOptions={{
        activeTintColor: colors.blue,
        inactiveTintColor: colors.silver200,
        indicatorStyle: { backgroundColor: colors.blue },
      }}>
      <ColumnTopTab.Screen
        name="My prayers"
        options={{ tabBarLabel: 'My prayers' }}>
        {() => (
          <PrayersScreen currentColumn={route.params} navigation={navigation} />
        )}
      </ColumnTopTab.Screen>
      <ColumnTopTab.Screen
        name="Subscribed"
        component={SubscribedScreen}
        options={{ tabBarLabel: 'Subscribed' }}
      />
    </ColumnTopTab.Navigator>
  );
};

export default ColumnTopTabNavigation;
