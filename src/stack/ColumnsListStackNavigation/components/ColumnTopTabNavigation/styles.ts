import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  tab: {
    height: 5,
  },
  indicator: {
    backgroundColor: colors.blue,
  },
});
