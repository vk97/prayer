import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  inputAdd: {
    borderRadius: 0,
    borderWidth: 0,
    paddingLeft: 48,
  },
  iconComment: {
    zIndex: 2,
    top: 17,
    left: 17,
  },
});
