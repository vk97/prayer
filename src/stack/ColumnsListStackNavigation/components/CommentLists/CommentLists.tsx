import IconComment from 'components/Icon/IconComment';
import InputAdd from 'components/InputAdd';
import React from 'react';
import { Field, Form } from 'react-final-form';
import { View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';
import { createCommentRequest, deleteCommentRequest } from 'redux/todo/actions';
import { selectCommentsCard } from 'redux/todo/selectors';
import { ICard, IComment } from 'types';
import Comment from '../Comment';
import { styles } from './styles';

interface CommentListsProps {
  card: ICard;
}

const CommentLists: React.FC<CommentListsProps> = ({ card }) => {
  const dispatch = useDispatch();
  const comments = useSelector(selectCommentsCard(card.id));

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={comments}
        keyExtractor={(comment) => comment.id.toString()}
        renderItem={({ item }: { item: IComment }) => (
          <Comment
            comment={item}
            onLongPress={() => {
              dispatch(deleteCommentRequest(item.id));
            }}
          />
        )}
        ListFooterComponent={() => (
          <View>
            <Form
              onSubmit={({ body }) => {
                dispatch(
                  createCommentRequest({
                    elementId: card.id,
                    data: {
                      body,
                      created: new Date().toString(),
                    },
                  }),
                );
              }}
              render={({ handleSubmit, values }) => (
                <View>
                  <Field name="body" placeholder="Add a comment...">
                    {({ meta, placeholder, input }) => {
                      return (
                        <InputAdd
                          value={values.body}
                          styleInput={styles.inputAdd}
                          meta={meta}
                          placeholder={placeholder}
                          onChange={input.onChange}
                          onSubmit={handleSubmit}>
                          <IconComment styles={styles.iconComment} />
                        </InputAdd>
                      );
                    }}
                  </Field>
                </View>
              )}
            />
          </View>
        )}
      />
    </SafeAreaView>
  );
};

export default CommentLists;
