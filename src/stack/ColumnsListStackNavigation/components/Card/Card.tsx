import React from 'react';
import { Animated, TouchableOpacity, View } from 'react-native';
import { CheckBox } from 'react-native-elements';
import AppText from 'components/AppText';
import IconLine from 'components/Icon/IconLine';
import IconCheckBoxOn from 'components/Icon/IconCheckBoxOn';
import IconCheckBoxOff from 'components/Icon/IconCheckBoxOff';
import { ICard, IColumn } from 'types';
import { styles } from './styles';
import CardInfo from '../CardInfo';
import { useDispatch, useSelector } from 'react-redux';
import { updateCardRequest } from 'redux/todo/actions';
import { selectCommentsCard } from 'redux/todo/selectors';

interface CardProps {
  column: IColumn;
  card: ICard;
  onPress: () => void;
}

const mockCountPrayed = 25;

const Card: React.FC<CardProps> = ({ column, card, onPress }) => {
  const comment = useSelector(selectCommentsCard(card.id));
  const dispatch = useDispatch();
  const { title, checked } = card;
  let stylesTextChecked;

  if (checked) {
    stylesTextChecked = styles.textCardChecked;
  }

  const updateCard = () => {
    if (!checked) {
      const newCard = {
        title: card.title,
        description: card.description,
        checked: true,
        column,
      };
      dispatch(updateCardRequest({ elementId: card.id, data: newCard }));
    }
  };

  return (
    <Animated.View style={styles.container}>
      <View style={styles.card}>
        <IconLine />
        <CheckBox
          containerStyle={styles.checkboxContainer}
          checked={checked}
          size={22}
          onPress={updateCard}
          checkedIcon={<IconCheckBoxOn />}
          uncheckedIcon={<IconCheckBoxOff />}
        />
        <View style={styles.buttonCardContainer}>
          <TouchableOpacity
            style={styles.buttonCard}
            onPress={onPress}
            activeOpacity={1}>
            <View style={styles.cardInfo}>
              <AppText style={stylesTextChecked}>
                {title.length < 13
                  ? `${title}`
                  : `${title.substring(0, 10)}...`}
              </AppText>
              <CardInfo
                countMembers={comment.length}
                countPrayed={mockCountPrayed}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Animated.View>
  );
};

export default Card;
