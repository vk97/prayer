import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
  },
  card: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 68,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: colors.silver100,
  },
  checkboxContainer: {
    padding: 0,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 18,
    marginRight: 15,
  },
  buttonCardContainer: {
    flex: 1,
  },
  cardInfo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonCard: {
    height: 65,
    justifyContent: 'center',
  },
  textCardChecked: {
    textDecorationLine: 'line-through',
  },
});
