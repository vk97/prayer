import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginLeft: 'auto',
  },
  totalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  totalValue: {
    fontSize: 12,
    lineHeight: 14,
    marginLeft: 5,
  },
});
