import AppText from 'components/AppText';
import IconPrayer from 'components/Icon/IconPrayer';
import IconUser from 'components/Icon/IconUser';
import React from 'react';
import { View } from 'react-native';
import { styles } from './styles';

interface CardInfoProps {
  countMembers: number;
  countPrayed: number;
}

const CardInfo: React.FC<CardInfoProps> = ({ countMembers, countPrayed }) => {
  let membersElement;
  let prayedElement;
  if (countMembers) {
    membersElement = (
      <View style={styles.totalContainer}>
        <IconUser />
        <AppText style={styles.totalValue}>{countMembers}</AppText>
      </View>
    );
  }

  if (countPrayed) {
    prayedElement = (
      <View style={styles.totalContainer}>
        <IconPrayer />
        <AppText style={styles.totalValue}>{countPrayed}</AppText>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      {membersElement}
      {prayedElement}
    </View>
  );
};

export default CardInfo;
