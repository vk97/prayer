import { StyleSheet } from 'react-native';
import colors from 'styles/colors';

export const styles = StyleSheet.create({
  headerStyle: {
    height: 64,
    borderBottomWidth: 1,
    borderBottomColor: colors.silver100,
    elevation: 0,
    shadowOpacity: 0,
  },
  headerTitleStyle: {
    fontFamily: 'SFUIText-Bold',
    fontSize: 17,
    lineHeight: 20,
    color: colors.silver400,
    textAlign: 'center',
  },
});
