import { PayloadAction } from '@reduxjs/toolkit';
import { put, call, takeEvery, select } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { deleteCardRequest } from '../actions';
import { deleteCard } from '../todoSlice';

function* fetchDeleteCard(action: PayloadAction<number | string>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.DELETE,
    token,
    `/cards/${action.payload}`,
  ]);
  if (response && action.payload) {
    yield put(deleteCard(action.payload));
  }
}

function* watchDeleteCard() {
  yield takeEvery(deleteCardRequest.toString(), fetchDeleteCard);
}

export { watchDeleteCard };
