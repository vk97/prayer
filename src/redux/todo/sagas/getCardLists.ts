import { takeEvery, put, call, select } from 'redux-saga/effects';
import { saveCardLists } from '../todoSlice';
import { getCardListsRequest } from '../actions';
import { loadCardsSuccess } from 'redux/application/applicationSlice';
import { RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { selectUserData } from 'redux/user/selectors';

function* fetchCardLists() {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [RequestType.GET, token, '/cards']);
  if (response) {
    const cards = response.data;
    yield put(saveCardLists(cards));
    yield put(loadCardsSuccess());
  }
}

function* watchGetCardLists() {
  yield takeEvery(getCardListsRequest.toString(), fetchCardLists);
}

export { watchGetCardLists };
