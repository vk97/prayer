import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { RequestData, RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { createColumnRequest } from '../actions';
import { addColumn } from '../todoSlice';

function* fetchCreateColumn(action: PayloadAction<RequestData>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.POST,
    token,
    '/columns',
    action.payload.data,
  ]);
  yield put(addColumn(response.data));
}

function* watchCreateColumn() {
  yield takeEvery(createColumnRequest.toString(), fetchCreateColumn);
}

export { watchCreateColumn };
