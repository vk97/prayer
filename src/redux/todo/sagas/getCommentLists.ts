import { takeEvery, put, call, select } from 'redux-saga/effects';
import { getCommentListsRequest } from '../actions';
import { saveCommentLists } from '../todoSlice';
import { RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { selectUserData } from 'redux/user/selectors';
import { loadCommentsSuccess } from 'redux/application/applicationSlice';

function* fetchCommentLists() {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [RequestType.GET, token, '/comments']);
  if (response) {
    const commentLists = response.data;
    yield put(saveCommentLists(commentLists));
    yield put(loadCommentsSuccess());
  }
}

function* watchGetCommentLists() {
  yield takeEvery(getCommentListsRequest.toString(), fetchCommentLists);
}

export { watchGetCommentLists };
