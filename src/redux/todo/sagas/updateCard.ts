import { PayloadAction } from '@reduxjs/toolkit';
import { put, call, takeEvery, select } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { ICard, RequestData, RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { updateCardRequest } from '../actions';
import { updateCard } from '../todoSlice';

function* fetchUpdateCard(action: PayloadAction<RequestData>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.PUT,
    token,
    `/cards/${action.payload.elementId}`,
    action.payload.data,
  ]);
  if (response.status === 200) {
    const card: ICard = response.data;
    yield put(updateCard(card));
  }
}

function* watchUpdateCard() {
  yield takeEvery(updateCardRequest.toString(), fetchUpdateCard);
}

export { watchUpdateCard };
