import { PayloadAction } from '@reduxjs/toolkit';
import { put, call, takeEvery, select } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { deleteCommentRequest } from '../actions';
import { deleteComment } from '../todoSlice';

function* fetchDeleteComment(action: PayloadAction<number | string>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.DELETE,
    token,
    `/comments/${action.payload}`,
  ]);
  if (response && action.payload) {
    yield put(deleteComment(action.payload));
  }
}

function* watchDeleteComment() {
  yield takeEvery(deleteCommentRequest.toString(), fetchDeleteComment);
}

export { watchDeleteComment };
