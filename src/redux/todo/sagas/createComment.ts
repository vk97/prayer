import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { RequestData, RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { createCommentRequest } from '../actions';
import { addComment } from '../todoSlice';

function* fetchCreateComment(action: PayloadAction<RequestData>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.POST,
    token,
    `/cards/${action.payload.elementId}/comments`,
    action.payload.data,
  ]);
  yield put(addComment(response.data));
}

function* watchCreateComment() {
  yield takeEvery(createCommentRequest.toString(), fetchCreateComment);
}

export { watchCreateComment };
