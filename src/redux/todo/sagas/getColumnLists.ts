import { takeEvery, put, call, select } from 'redux-saga/effects';
import { getColumnListsRequest } from '../actions';
import { saveColumnLists } from '../todoSlice';
import { loadColumnsSuccess } from 'redux/application/applicationSlice';
import { RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { selectUserData } from 'redux/user/selectors';

function* fetchColumnLists() {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [RequestType.GET, token, '/columns']);
  if (response) {
    const columnList = response.data;
    yield put(saveColumnLists(columnList));
    yield put(loadColumnsSuccess());
  }
}

function* watchGetColumnLists() {
  yield takeEvery(getColumnListsRequest.toString(), fetchColumnLists);
}

export { watchGetColumnLists };
