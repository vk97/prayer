import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { RequestData, RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { createCardRequest } from '../actions';
import { addCard } from '../todoSlice';

function* fetchCreateCard(action: PayloadAction<RequestData>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.POST,
    token,
    '/cards',
    action.payload.data,
  ]);
  yield put(addCard(response.data));
}

function* watchCreateCard() {
  yield takeEvery(createCardRequest.toString(), fetchCreateCard);
}

export { watchCreateCard };
