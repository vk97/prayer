import { PayloadAction } from '@reduxjs/toolkit';
import { put, call, takeEvery, select } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { deleteColumnRequest } from '../actions';
import { deleteColumn } from '../todoSlice';

function* fetchDeleteColumn(action: PayloadAction<number | string>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.DELETE,
    token,
    `/columns/${action.payload}`,
  ]);
  if (response && action.payload) {
    yield put(deleteColumn(action.payload));
  }
}

function* watchDeleteColumn() {
  yield takeEvery(deleteColumnRequest.toString(), fetchDeleteColumn);
}

export { watchDeleteColumn };
