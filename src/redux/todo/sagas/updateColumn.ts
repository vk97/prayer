import { PayloadAction } from '@reduxjs/toolkit';
import { put, call, takeEvery, select } from 'redux-saga/effects';
import { selectUserData } from 'redux/user/selectors';
import { IColumn, RequestData, RequestType } from 'types';
import fetchApi from 'utils/fetchApi';
import { updateColumnRequest } from '../actions';
import { updateColumn } from '../todoSlice';

function* fetchUpdateColumn(action: PayloadAction<RequestData>) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  const response = yield call(fetchApi, [
    RequestType.PUT,
    token,
    `/columns/${action.payload.elementId}`,
    action.payload.data,
  ]);
  if (response.status === 200 && action.payload.elementId) {
    const column: IColumn = response.data;
    yield put(updateColumn(column));
  }
}

function* watchUpdateColumn() {
  yield takeEvery(updateColumnRequest.toString(), fetchUpdateColumn);
}

export { watchUpdateColumn };
