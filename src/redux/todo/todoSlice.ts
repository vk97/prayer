import {
  createSlice,
  PayloadAction,
  createEntityAdapter,
} from '@reduxjs/toolkit';
import { ICard, IColumn, IComment } from 'types';

export const columnAdapter = createEntityAdapter<IColumn>({
  selectId: (column) => column.id,
  sortComparer: false,
});

export const cardAdapter = createEntityAdapter<ICard>({
  selectId: (card) => card.id,
  sortComparer: false,
});

export const commentAdapter = createEntityAdapter<IComment>({
  selectId: (card) => card.id,
  sortComparer: false,
});

const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    columnLists: columnAdapter.getInitialState(),
    cardLists: cardAdapter.getInitialState(),
    comments: commentAdapter.getInitialState(),
  },
  reducers: {
    saveColumnLists(state, action) {
      columnAdapter.setAll(state.columnLists, action.payload);
    },
    saveCardLists(state, action) {
      cardAdapter.setAll(state.cardLists, action.payload);
    },
    addColumn(state, action: PayloadAction<IColumn>) {
      columnAdapter.addOne(state.columnLists, action.payload);
    },
    deleteColumn(state, action: PayloadAction<number | string>) {
      columnAdapter.removeOne(state.columnLists, action.payload);
    },
    updateColumn(state, action: PayloadAction<IColumn>) {
      columnAdapter.upsertOne(state.columnLists, action.payload);
    },
    addCard(state, action: PayloadAction<ICard>) {
      cardAdapter.addOne(state.cardLists, action.payload);
    },
    deleteCard(state, action: PayloadAction<number | string>) {
      cardAdapter.removeOne(state.cardLists, action.payload);
    },
    updateCard(state, action: PayloadAction<ICard>) {
      cardAdapter.upsertOne(state.cardLists, action.payload);
    },
    saveCommentLists(state, action) {
      commentAdapter.setAll(state.comments, action.payload);
    },
    addComment(state, action: PayloadAction<IComment>) {
      commentAdapter.addOne(state.comments, action.payload);
    },
    updateComment(state, action: PayloadAction<IComment>) {
      commentAdapter.upsertOne(state.comments, action.payload);
    },
    deleteComment(state, action: PayloadAction<number | string>) {
      commentAdapter.removeOne(state.comments, action.payload);
    },
  },
});

export const {
  saveColumnLists,
  saveCardLists,
  addColumn,
  deleteColumn,
  addCard,
  updateColumn,
  saveCommentLists,
  addComment,
  updateComment,
  deleteComment,
  deleteCard,
  updateCard,
} = todoSlice.actions;
export default todoSlice.reducer;
