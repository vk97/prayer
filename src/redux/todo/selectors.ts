import { RootState } from 'redux/reducer';
import { ICard } from 'types';
import { columnAdapter, cardAdapter, commentAdapter } from './todoSlice';

export const columnSelector = columnAdapter.getSelectors();
export const cardSelector = cardAdapter.getSelectors();
export const commentSelector = commentAdapter.getSelectors();

export const selectColumns = (state: RootState) =>
  columnSelector.selectAll(state.TODO.columnLists);
export const selectCards = (state: RootState) =>
  cardSelector.selectAll(state.TODO.cardLists);
export const selectComments = (state: RootState) =>
  commentSelector.selectAll(state.TODO.comments);
export const selectCommentsCard = (currentCardId: number) => (
  state: RootState,
) => {
  return commentSelector
    .selectAll(state.TODO.comments)
    .filter((comment) => comment.cardId === currentCardId);
};
export const selectColumnCards = (currentColumn: number) => (
  state: RootState,
): Array<ICard> | [] => {
  const cardLists = cardSelector.selectAll(state.TODO.cardLists);

  if (cardLists.length) {
    return cardLists.filter((card) => card.columnId === currentColumn);
  } else {
    return [];
  }
};
