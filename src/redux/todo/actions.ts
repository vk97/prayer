import { createAction } from '@reduxjs/toolkit';
import { RequestData } from 'types';
import {
  GET_COLUMN_LISTS_REQUEST,
  GET_CARD_LISTS_REQUEST,
  CREATE_COLUMN_LIST_REQUEST,
  CREATE_CARD_REQUEST,
  DELETE_COLUMN_REQUEST,
  UPDATE_COLUMN_REQUEST,
  GET_COMMENT_LISTS_REQUEST,
  UPDATE_COMMENT_REQUEST,
  DELETE_COMMENT_REQUEST,
  CREATE_COMMENT_REQUEST,
  DELETE_CARD_REQUEST,
  UPDATE_CARD_REQUEST,
} from './types';

export const getColumnListsRequest = createAction(GET_COLUMN_LISTS_REQUEST);
export const getCardListsRequest = createAction(GET_CARD_LISTS_REQUEST);
export const getCommentListsRequest = createAction(GET_COMMENT_LISTS_REQUEST);
export const createColumnRequest = createAction<RequestData>(
  CREATE_COLUMN_LIST_REQUEST,
);
export const createCardRequest = createAction<RequestData>(CREATE_CARD_REQUEST);
export const deleteCardRequest = createAction<number | string>(
  DELETE_CARD_REQUEST,
);
export const updateCardRequest = createAction<RequestData>(UPDATE_CARD_REQUEST);
export const deleteColumnRequest = createAction<number | string>(
  DELETE_COLUMN_REQUEST,
);
export const updateColumnRequest = createAction<RequestData>(
  UPDATE_COLUMN_REQUEST,
);
export const createCommentRequest = createAction<RequestData>(
  CREATE_COMMENT_REQUEST,
);
export const updateCommentRequest = createAction<RequestData>(
  UPDATE_COMMENT_REQUEST,
);
export const deleteCommentRequest = createAction<number | string>(
  DELETE_COMMENT_REQUEST,
);
