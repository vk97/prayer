import { combineReducers } from 'redux';
import { AnyAction, Reducer } from '@reduxjs/toolkit';
import todoSlice from './todo/todoSlice';
import userReducer from './user/userSlice';
import applicationReducer from './application/applicationSlice';
import { NAME_SPACE } from './namespace';
import { userLogout } from './user/actions';

const combinedReducer = combineReducers({
  [NAME_SPACE.TODO]: todoSlice,
  [NAME_SPACE.USER]: userReducer,
  [NAME_SPACE.APPLICATION]: applicationReducer,
});

export type RootState = ReturnType<typeof combinedReducer>;

const rootReducer: Reducer = (state: RootState, action: AnyAction) => {
  if (action.type === userLogout.toString()) {
    state = {} as RootState;
  }
  return combinedReducer(state, action);
};

export default rootReducer;
