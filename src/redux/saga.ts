import { all } from 'redux-saga/effects';
import {
  watchCheckAuthUser,
  watchRegistrationUser,
} from 'redux/user/sagas/auth';
import { watchGetColumnLists } from 'redux/todo/sagas/getColumnLists';
import { watchGetCardLists } from 'redux/todo/sagas/getCardLists';
import { watchCreateColumn } from 'redux/todo/sagas/createColumn';
import { watchCreateCard } from 'redux/todo/sagas/createCard';
import { watchDeleteColumn } from 'redux/todo/sagas/deleteColumn';
import { watchUpdateColumn } from 'redux/todo/sagas/updateColumn';
import { watchGetCommentLists } from 'redux/todo/sagas/getCommentLists';
import { watchCreateComment } from 'redux/todo/sagas/createComment';
import { watchDeleteComment } from 'redux/todo/sagas/deleteComment';
import { watchDeleteCard } from 'redux/todo/sagas/deleteCard';
import { watchUpdateCard } from 'redux/todo/sagas/updateCard';

export default function* rootSaga() {
  yield all([
    watchCheckAuthUser(),
    watchRegistrationUser(),
    watchGetColumnLists(),
    watchGetCardLists(),
    watchCreateColumn(),
    watchCreateCard(),
    watchDeleteColumn(),
    watchUpdateColumn(),
    watchGetCommentLists(),
    watchCreateComment(),
    watchDeleteComment(),
    watchDeleteCard(),
    watchUpdateCard(),
  ]);
}
