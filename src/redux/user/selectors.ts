import { RootState } from 'redux/reducer';

export const selectUserData = (state: RootState) => state.USER.userData;
export const selectAuthStatus = (state: RootState) => state.USER.authStatus;
export const selectStatusLoadingUserAuth = (state: RootState) =>
  state.USER.isLoadingUserAuth;
export const selectStatusLoadedUserAuth = (state: RootState) =>
  state.USER.isLoadedUserAuth;
