import { put, takeEvery, call, select } from 'redux-saga/effects';
import { saveColumnLists } from 'redux/todo/todoSlice';
import { authorizeUser, changeStatusLoading } from 'redux/user/userSlice';
import { signInRequest, signUpRequest } from '../actions';
import { RegisterUser, ActionUserPayload, CheckAuthorize } from '../types';
import { RequestType } from 'types';
import { Alert } from 'react-native';
import fetchApi from 'utils/fetchApi';
import { selectUserData } from '../selectors';

function* fetchAuthorizeUser(action: CheckAuthorize) {
  const userData = yield select(selectUserData);
  const { token } = userData;
  try {
    const response = yield call(fetchApi, [
      RequestType.POST,
      token,
      '/auth/sign-in',
      action.payload,
    ]);

    if (response) {
      if (response.data.message) {
        Alert.alert('Incorrect username or password');
        yield put(changeStatusLoading(false));
      } else if (response.data.hasOwnProperty('token')) {
        yield put(authorizeUser(response.data));
      }
    }
  } catch (error) {
    yield put(changeStatusLoading(false));
  }
}

function* registrationUser(action: RegisterUser) {
  const userDataStore = yield select(selectUserData);
  const { tokenStore } = userDataStore;
  try {
    const response = yield call(fetchApi, [
      RequestType.POST,
      tokenStore,
      '/auth/sign-up',
      action.payload,
    ]);

    if (response) {
      if (response.data.message) {
        yield put(changeStatusLoading(false));
        Alert.alert('This user already exists. Please log in');
      } else if (response.data.hasOwnProperty('token')) {
        const { id, name, email, token, columns } = response.data;
        const userData: ActionUserPayload = {
          id,
          email,
          name,
          token,
        };
        yield put(authorizeUser(userData));
        yield put(saveColumnLists(columns));
      }
    }
  } catch (error) {
    yield put(changeStatusLoading(false));
  }
}

function* watchCheckAuthUser() {
  yield takeEvery(signInRequest.toString(), fetchAuthorizeUser);
}

function* watchRegistrationUser() {
  yield takeEvery(signUpRequest.toString(), registrationUser);
}
export { watchCheckAuthUser, watchRegistrationUser };
