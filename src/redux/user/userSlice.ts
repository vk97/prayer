import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AuthorizeStatus } from '../../types/index';
import { ActionUserPayload, UserState } from './types';

const initialState: UserState = {
  userData: {
    id: -1,
    email: '',
    name: '',
    token: null,
  },
  authStatus: AuthorizeStatus.USER_NO_AUTH,
  isLoadingUserAuth: false,
  isLoadedUserAuth: false,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    changeStatusLoading(state, action) {
      state.isLoadingUserAuth = action.payload;
    },
    changeStatusLoaded(state, action) {
      state.isLoadedUserAuth = action.payload;
    },
    authorizeUser(state, action: PayloadAction<ActionUserPayload>) {
      state.userData = action.payload;
      state.authStatus = AuthorizeStatus.USER_AUTH;
      state.isLoadedUserAuth = true;
      state.isLoadingUserAuth = false;
    },
  },
});

export const {
  changeStatusLoading,
  changeStatusLoaded,
  authorizeUser,
} = userSlice.actions;
export default userSlice.reducer;
