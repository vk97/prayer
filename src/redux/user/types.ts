import { AuthorizeStatus, IUserData } from 'types/index';

export const USER_LOGOUT = 'USER_LOGOUT';
export const SIGN_IN_REQUEST = 'AUTH_REQUEST';
export const SIGN_UP_REQUEST = 'REGISTER_USER';

export interface UserState {
  userData: IUserData;
  authStatus: AuthorizeStatus;
  isLoadingUserAuth: boolean;
  isLoadedUserAuth: boolean;
}

export interface ActionUserPayload {
  id: number;
  name: string;
  email: string;
  token: string;
}

export interface UserAuth {
  email: string;
  password: string;
}

export interface CheckAuthorize {
  type: typeof SIGN_IN_REQUEST;
  payload: UserAuth;
}

export interface RegisterUser {
  type: typeof SIGN_UP_REQUEST;
  payload: IUserData;
}
