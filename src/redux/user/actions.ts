import { createAction } from '@reduxjs/toolkit';
import { IUserData } from 'types';
import {
  SIGN_IN_REQUEST,
  SIGN_UP_REQUEST,
  UserAuth,
  USER_LOGOUT,
} from './types';

export const signInRequest = createAction<UserAuth>(SIGN_IN_REQUEST);
export const signUpRequest = createAction<IUserData>(SIGN_UP_REQUEST);
export const userLogout = createAction(USER_LOGOUT);
