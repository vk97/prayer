export interface ApplicationState {
  isLoadedColumns: boolean;
  isLoadedCards: boolean;
  isLoadedComments: boolean;
}
