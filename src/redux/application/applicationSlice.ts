import { createSlice } from '@reduxjs/toolkit';
import { ApplicationState } from './types';

const initialState: ApplicationState = {
  isLoadedColumns: false,
  isLoadedCards: false,
  isLoadedComments: false,
};

const applicationSlice = createSlice({
  name: 'application',
  initialState,
  reducers: {
    loadColumnsSuccess(state) {
      state.isLoadedColumns = true;
    },
    loadCardsSuccess(state) {
      state.isLoadedCards = true;
    },
    loadCommentsSuccess(state) {
      state.isLoadedComments = true;
    },
  },
});

export const {
  loadColumnsSuccess,
  loadCardsSuccess,
  loadCommentsSuccess,
} = applicationSlice.actions;
export default applicationSlice.reducer;
