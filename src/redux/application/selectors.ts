import { RootState } from 'redux/reducer';

export const selectStatusLoadedColumns = (state: RootState) => {
  return state.APPLICATION.isLoadedColumns;
};

export const selectStatusLoadedCards = (state: RootState) =>
  state.APPLICATION.isLoadedCards;

export const selectStatusLoadedComments = (state: RootState) =>
  state.APPLICATION.isLoadedComments;
